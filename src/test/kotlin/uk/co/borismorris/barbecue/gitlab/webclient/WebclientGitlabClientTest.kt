package uk.co.borismorris.barbecue.gitlab.webclient

import com.github.jenspiegsa.wiremockextension.ConfigureWireMock
import com.github.jenspiegsa.wiremockextension.InjectServer
import com.github.jenspiegsa.wiremockextension.WireMockExtension
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.Options.ChunkedEncodingPolicy.NEVER
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.SoftAssertions.assertSoftly
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.core.io.ByteArrayResource
import org.springframework.web.reactive.function.client.WebClient
import uk.co.borismorris.barbecue.gitlab.Commit
import uk.co.borismorris.barbecue.gitlab.GitlabConfig
import uk.co.borismorris.barbecue.gitlab.Job
import uk.co.borismorris.barbecue.gitlab.Pipeline
import uk.co.borismorris.barbecue.gitlab.Runner
import uk.co.borismorris.barbecue.gitlab.Status
import uk.co.borismorris.barbecue.gitlab.User
import java.math.BigDecimal
import java.net.InetAddress
import java.net.URL
import java.time.LocalDateTime.now

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(WireMockExtension::class)
internal class WebclientGitlabClientTest {
    val localhost = URL("http://localhost")

    lateinit var config: GitlabConfig
    val webclient = WebClient.builder()

    @InjectServer
    lateinit var serverMock: WireMockServer

    @ConfigureWireMock
    @SuppressWarnings("unused")
    val options = WireMockConfiguration.wireMockConfig()
            .dynamicPort()
            .usingFilesUnderClasspath("uk/co/borismorris/barbecue/gitlab/webclient/wiremock")
            .useChunkedTransferEncoding(NEVER)
            .notifier(ConsoleNotifier(false))

    @BeforeEach
    fun init() {
        config = GitlabConfig().apply {
            url = URL("http://localhost:${serverMock.port()}")
            project = "bbq-themometer/thermometer"
        }
    }

    @Test
    fun `When I request the most recent pipeline with a valid token then the response is correctly parsed`() = runBlocking<Unit> {
        config.token = "SECRET-TOKEN"
        val gitlabClient = WebclientGitlabClient(config, webclient)

        val pipeline = gitlabClient.mostRecentPipeline()

        assertSoftly {
            with(pipeline) {
                it.assertThat(id).isEqualTo("65216167")
                it.assertThat(sha).isEqualTo("80155ce219b4932f7ea17f37152e501ec892c2ac")
                it.assertThat(ref).isEqualTo("master")
                it.assertThat(status).isEqualTo(Status.SUCCESS)
            }
        }
    }

    @Test
    fun `When I request the jobs for a pipeline with a valid token then the response is correctly parsed`() = runBlocking<Unit> {
        config.token = "SECRET-TOKEN"
        val gitlabClient = WebclientGitlabClient(config, webclient)
        val pipeline = Pipeline("65216167", "", "", Status.SUCCESS, localhost)

        val jobs = gitlabClient.jobs(pipeline).toList()

        assertThat(jobs).hasOnlyOneElementSatisfying {
            with(it) {
                assertThat(id).isEqualTo("227345547")
                assertThat(ref).isEqualTo("master")
                assertThat(status).isEqualTo(Status.SUCCESS)
            }
        }
    }

    @Test
    fun `When I request an artefact from a job with a valid token then I receive a stream of the artefact archive`() = runBlocking<Unit> {
        config.token = "SECRET-TOKEN"
        val gitlabClient = WebclientGitlabClient(config, webclient)
        val job = simpleJob("12345")

        val artefactData = gitlabClient.artefact(job)

        assertSoftly { softly ->
            softly.assertThat(artefactData.resource.contentLength()).isEqualTo(3600L)
            softly.assertThat(artefactData.resource).isInstanceOfSatisfying(ByteArrayResource::class.java) {
                val data = it.byteArray.decodeToString()
                assertThat(data)
                        .hasSize(3600)
                        .startsWith("Lorem ipsum dolor sit amet")
                        .endsWith("quam vulputate dignissim suspendisse.")
            }
        }
    }

    @MethodSource("testArtefactPaths")
    @ParameterizedTest
    fun `When I request an artefact file from a job with a valid token then I receive a stream of the artefact file`(id: String, path: String?) = runBlocking<Unit> {
        config.token = "SECRET-TOKEN"
        val gitlabClient = WebclientGitlabClient(config, webclient)
        val job = simpleJob(id)

        val artefactData = gitlabClient.artefact(job, path)

        assertSoftly { softly ->
            softly.assertThat(artefactData.resource.contentLength()).isEqualTo(3600L)
            softly.assertThat(artefactData.resource).isInstanceOfSatisfying(ByteArrayResource::class.java) {
                val data = it.byteArray.decodeToString()
                assertThat(data)
                        .hasSize(3600)
                        .startsWith("Lorem ipsum dolor sit amet")
                        .endsWith("quam vulputate dignissim suspendisse.")
            }
        }
    }

    fun testArtefactPaths() = listOf(
            arguments("12345", null),
            arguments("12345", ""),
            arguments("12345", "/"),
            arguments("54321", "path/to/artefact"),
            arguments("54321", "/path/to/artefact")
    )

    fun simpleJob(id: String) = Job(
            false,
            listOf(),
            now(),
            Commit("", "", now(), now(), "", "", now(), "", "", listOf(), "", ""),
            "",
            now(),
            BigDecimal.ZERO,
            now(),
            id,
            "",
            Pipeline("", "", "", Status.SUCCESS, localhost),
            "",
            Runner(true, "", "", InetAddress.getLoopbackAddress(), true, "", true, ""),
            "",
            now(),
            Status.SUCCESS,
            false,
            User(localhost, "", now(), "", "", "", "", "", "", "", "", "", "", localhost, ""),
            localhost)
}
