package uk.co.borismorris.barbecue

import org.springframework.boot.WebApplicationType.REACTIVE
import org.springframework.boot.autoconfigure.info.ProjectInfoAutoConfiguration
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientCodecCustomizer
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.logging.LogLevel
import org.springframework.boot.web.codec.CodecCustomizer
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.fu.kofu.application
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.reactiveWebApplication
import org.springframework.fu.kofu.webflux.webFlux
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.http.codec.ResourceHttpMessageWriter
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import uk.co.borismorris.barbecue.gitlab.GitlabConfig
import uk.co.borismorris.barbecue.gitlab.webclient.WebclientGitlabClient
import uk.co.borismorris.barbecue.logging.LogConfigOnStartup
import uk.co.borismorris.barbecue.logging.LogGitInfoOnStartup
import uk.co.borismorris.barbecue.logging.LogVersionOnStartup
import uk.co.borismorris.barbecue.otaupdate.OtaUpdateHandler
import uk.co.borismorris.barbecue.temperature.TemperatureHandler
import uk.co.borismorris.barbecue.temperature.TemperatureReadingConverter
import uk.co.borismorris.barbecue.web.RateLimitFilter
import uk.co.borismorris.barbecue.web.routes
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.webclient.WebclientInfluxClient

fun main(args: Array<String>) {
    app.run(args)
}

val app = reactiveWebApplication {
    enable(webClientConfig)
    enable(gitlabClientConfig)
    enable(influxClientConfig)
    enable(startupLoggingConfig)

    enable(configureWebfluxCodecs)

    beans {
        bean<OtaUpdateHandler>()
        bean<TemperatureReadingConverter>()
        bean<TemperatureHandler>()
        bean(::routes)
    }

    webFlux {
        port = if (profiles.contains("test")) 0 else 8080
        codecs {
            string(true)
            jackson()
        }
        filter<RateLimitFilter>()
    }

    logging {
        level = LogLevel.INFO
        //level("org.springframework.web", LogLevel.TRACE)
        //level("uk.co.borismorris", LogLevel.TRACE)
    }
}

val webClientConfig = configuration {
    beans {
        bean { followRedirects() }
        bean<WebClientCodecCustomizer>()
        bean(scope = BeanDefinitionDsl.Scope.PROTOTYPE) {
            val customizerProvider = provider<WebClientCustomizer>()
            val builder = WebClient.builder()
            customizerProvider.forEach { customizer -> customizer.customize(builder) }
            builder
        }
    }
}

private fun followRedirects() = WebClientCustomizer {
    it.clientConnector(ReactorClientHttpConnector(httpClient()))
}

private fun httpClient() = HttpClient.create()
        .followRedirect(true)
        .keepAlive(true)

val gitlabClientConfig = configuration {
    configurationProperties<GitlabConfig>(prefix = "gitlab")
    beans {
        bean<WebclientGitlabClient>()
    }
}

val influxClientConfig = configuration {
    configurationProperties<InfluxDbProps>(prefix = "influxdb")
    beans {
        bean<WebclientInfluxClient>()
    }
}

val startupLoggingConfig = configuration {
    beans {
        profile("prod") {
            bean { ProjectInfoAutoConfiguration(ProjectInfoProperties()) }
            bean { ref<ProjectInfoAutoConfiguration>().buildProperties() }
            bean { ref<ProjectInfoAutoConfiguration>().gitProperties() }
        }
        bean<LogConfigOnStartup>()
        bean<LogVersionOnStartup>()
        bean<LogGitInfoOnStartup>()
    }
    listener<ApplicationStartedEvent> {
        ref<LogConfigOnStartup>().onStartup()
        ref<LogVersionOnStartup>().onStartup()
        ref<LogGitInfoOnStartup>().onStartup()
    }
}

val configureWebfluxCodecs = configuration {
    beans {
        bean {
            CodecCustomizer {
                it.defaultCodecs().apply {
                    maxInMemorySize(1 shl 20)
                }
                it.customCodecs().register(ResourceHttpMessageWriter())
            }
        }
    }
}
