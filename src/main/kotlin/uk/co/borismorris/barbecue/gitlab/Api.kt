package uk.co.borismorris.barbecue.gitlab

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.coroutines.flow.Flow
import org.springframework.core.io.Resource
import java.math.BigDecimal
import java.net.InetAddress
import java.net.URL
import java.time.LocalDateTime

enum class Status {
    @JsonProperty("running")
    RUNNING,

    @JsonProperty("pending")
    PENDING,

    @JsonProperty("success")
    SUCCESS,

    @JsonProperty("failed")
    FAILED,

    @JsonProperty("canceled")
    CANCELED,

    @JsonProperty("skipped")
    SKIPPED
}

data class Job(
        @JsonProperty("allow_failure")
        val allowFailure: Boolean,
        @JsonProperty("artifacts")
        @JsonManagedReference
        val artifacts: List<Artifact>,
        @JsonProperty("artifacts_expire_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val artifactsExpireAt: LocalDateTime,
        @JsonProperty("commit")
        val commit: Commit,
        @JsonProperty("coverage")
        val coverage: String?,
        @JsonProperty("created_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val createdAt: LocalDateTime,
        @JsonProperty("duration")
        val duration: BigDecimal,
        @JsonProperty("finished_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val finishedAt: LocalDateTime,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("pipeline")
        val pipeline: Pipeline,
        @JsonProperty("ref")
        val ref: String,
        @JsonProperty("runner")
        val runner: Runner,
        @JsonProperty("stage")
        val stage: String,
        @JsonProperty("started_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val startedAt: LocalDateTime,
        @JsonProperty("status")
        val status: Status,
        @JsonProperty("tag")
        val tag: Boolean,
        @JsonProperty("user")
        val user: User,
        @JsonProperty("web_url")
        val webUrl: URL
)

data class Pipeline(
        @JsonProperty("id")
        val id: String,
        @JsonProperty("ref")
        val ref: String,
        @JsonProperty("sha")
        val sha: String,
        @JsonProperty("status")
        val status: Status,
        @JsonProperty("web_url")
        val webUrl: URL
)

data class User(
        @JsonProperty("avatar_url")
        val avatarUrl: URL,
        @JsonProperty("bio")
        val bio: String?,
        @JsonProperty("created_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val createdAt: LocalDateTime,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("linkedin")
        val linkedin: String,
        @JsonProperty("location")
        val location: String?,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("organization")
        val organization: String?,
        @JsonProperty("public_email")
        val publicEmail: String,
        @JsonProperty("skype")
        val skype: String,
        @JsonProperty("state")
        val state: String,
        @JsonProperty("twitter")
        val twitter: String,
        @JsonProperty("username")
        val username: String,
        @JsonProperty("web_url")
        val webUrl: URL,
        @JsonProperty("website_url")
        val websiteUrl: String
)

data class Artifact(
        @JsonProperty("file_format")
        val fileFormat: String?,
        @JsonProperty("file_type")
        val fileType: String,
        @JsonProperty("filename")
        val filename: String,
        @JsonProperty("size")
        val size: Long
)

data class Runner(
        @JsonProperty("active")
        val active: Boolean,
        @JsonProperty("description")
        val description: String,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("ip_address")
        val ipAddress: InetAddress,
        @JsonProperty("is_shared")
        val isShared: Boolean,
        @JsonProperty("name")
        val name: String,
        @JsonProperty("online")
        val online: Boolean,
        @JsonProperty("status")
        val status: String
)

data class Commit(
        @JsonProperty("author_email")
        val authorEmail: String,
        @JsonProperty("author_name")
        val authorName: String,
        @JsonProperty("authored_date")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val authoredDate: LocalDateTime,
        @JsonProperty("committed_date")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val committedDate: LocalDateTime,
        @JsonProperty("committer_email")
        val committerEmail: String,
        @JsonProperty("committer_name")
        val committerName: String,
        @JsonProperty("created_at")
        @JsonFormat(shape = STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
        val createdAt: LocalDateTime,
        @JsonProperty("id")
        val id: String,
        @JsonProperty("message")
        val message: String,
        @JsonProperty("parent_ids")
        val parentIds: List<String>,
        @JsonProperty("short_id")
        val shortId: String,
        @JsonProperty("title")
        val title: String
)

data class Artefact(val resource: Resource)

interface GitlabApi {
    suspend fun mostRecentPipeline(): Pipeline
    suspend fun jobs(pipeline: Pipeline): Flow<Job>
    suspend fun artefact(job: Job, path: String?): Artefact
    suspend fun artefact(job: Job) = artefact(job, "")
}
