package uk.co.borismorris.barbecue.gitlab

import java.net.URL

class GitlabConfig {
    lateinit var url: URL
    lateinit var project: String
    lateinit var token: String
}