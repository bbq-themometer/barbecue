package uk.co.borismorris.barbecue.web

import org.springframework.web.reactive.function.server.coRouter
import uk.co.borismorris.barbecue.otaupdate.OtaUpdateHandler
import uk.co.borismorris.barbecue.temperature.TemperatureHandler

fun routes(otaUpdateHandler: OtaUpdateHandler, temperatureHandler: TemperatureHandler) = coRouter {
    GET("/otaupdate", otaUpdateHandler::otaUpdate)
    PUT("/temperature", temperatureHandler::putTemperature)
}
