package uk.co.borismorris.barbecue

import com.github.jenspiegsa.wiremockextension.ConfigureWireMock
import com.github.jenspiegsa.wiremockextension.InjectServer
import com.github.jenspiegsa.wiremockextension.WireMockExtension
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.Options
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.http.HttpStatus.TOO_MANY_REQUESTS
import org.springframework.test.web.reactive.server.FluxExchangeResult
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(WireMockExtension::class)
internal class TemperatureRecordingTest {
    lateinit var context: ConfigurableApplicationContext
    lateinit var client: WebTestClient

    @InjectServer
    lateinit var serverMock: WireMockServer

    @ConfigureWireMock
    @SuppressWarnings("unused")
    val options = WireMockConfiguration.wireMockConfig()
            .dynamicPort()
            .usingFilesUnderClasspath("uk/co/borismorris/barbecue/gitlab/temperature/wiremock")
            .useChunkedTransferEncoding(Options.ChunkedEncodingPolicy.NEVER)
            .notifier(ConsoleNotifier(false))

    @BeforeEach
    fun init() {
        val args = arrayOf(
                "--gitlab.token=tokentoken",
                "--gitlab.url=http://localhost:${serverMock.port()}/gitlab",
                "--influxdb.url=http://localhost:${serverMock.port()}/influx"
        )
        context = app.run(args, "test", true)
        client = WebTestClient.bindToApplicationContext(context).build()
    }

    @AfterEach
    fun tearDown() {
        context.close()
    }

    @Test
    fun `What a valid measurement is posted it is recorded to influx`() {
        client.put()
                .uri("/temperature")
                .bodyValue("0124;1132.1")
                .exchange()
                .expectStatus().isEqualTo(NO_CONTENT)

        assertThat(getAllScenarios()).hasOnlyOneElementSatisfying { scenario ->
            assertThat(scenario.state).isEqualTo("Second measurement received")
        }
    }

    @Test
    fun `When multiple measurements are posted then the ratelimit is reached`() = runBlocking<Unit> {
        val results = coroutineScope {
            (1..10).map {
                async<FluxExchangeResult<String>> {
                    client.put()
                            .uri("/temperature")
                            .bodyValue("1125.0")
                            .exchange()
                            .returnResult<String>()
                }.await()
            }
        }
        val errors = results.groupBy { it.status }

        assertThat(errors).hasSize(2)
                .containsOnlyKeys(NO_CONTENT, TOO_MANY_REQUESTS)

        assertThat(errors[NO_CONTENT]).isNotEmpty().allSatisfy {
            assertThat(it.responseBody.collectList().block()).isEmpty()
            assertThat(it.responseHeaders.toMap()).containsKey("RateLimit-Remaining")
        }
        assertThat(errors[TOO_MANY_REQUESTS]).isNotEmpty().allSatisfy {
            assertThat(it.responseBody.collectList().block()).isEmpty()
            assertThat(it.responseHeaders.toMap()).containsKey("RateLimit-Remaining")
            assertThat(it.responseHeaders.toMap()).containsKey("RateLimit-Reset")
        }
    }
}
