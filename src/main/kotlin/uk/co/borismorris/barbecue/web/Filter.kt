package uk.co.borismorris.barbecue.web

import io.github.bucket4j.AsyncBucket
import io.github.bucket4j.Bandwidth
import io.github.bucket4j.Bucket4j
import org.springframework.http.HttpStatus.TOO_MANY_REQUESTS
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.util.AntPathMatcher
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono
import java.time.Duration
import java.util.concurrent.TimeUnit

class RateLimitFilter : WebFilter {
    private val matcher = AntPathMatcher()
    private val buckets = mapOf(
            "/otaupdate" to Bucket4j.builder()
                    .addLimit(Bandwidth.simple(1, Duration.ofMinutes(1)))
                    .build()
                    .asAsync(),
            "/temperature" to Bucket4j.builder()
                    .addLimit(Bandwidth.simple(5, Duration.ofSeconds(1)))
                    .build()
                    .asAsync()
    )

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain) = findBucketOrNull(exchange.request)
            ?.rateLimit(exchange, chain)
            ?: chain.filter(exchange)

    private fun findBucketOrNull(request: ServerHttpRequest) = buckets.asSequence()
            .filter { matcher.match(it.key, request.path.value()) }
            .map { it.value }
            .firstOrNull()

    private fun AsyncBucket.rateLimit(exchange: ServerWebExchange, chain: WebFilterChain) = tryConsumeAndReturnRemaining().flatMap { probe ->
        exchange.response.headers.set("RateLimit-Remaining", probe.remainingTokens.toString())
        if (probe.isConsumed) {
            chain.filter(exchange)
        } else {
            with(exchange.response) {
                statusCode = TOO_MANY_REQUESTS
                headers.set("RateLimit-Reset", TimeUnit.NANOSECONDS.toSeconds(probe.nanosToWaitForRefill).toString())
                setComplete()
            }
        }
    }

    private fun AsyncBucket.tryConsumeAndReturnRemaining() = Mono.fromFuture(tryConsumeAndReturnRemaining(1))
}
