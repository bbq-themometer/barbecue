package uk.co.borismorris.barbecue.logging

import mu.KLogging
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.context.ConfigurableApplicationContext
import uk.co.borismorris.barbecue.gitlab.GitlabConfig
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps

class LogConfigOnStartup(private val context: ConfigurableApplicationContext) {

    companion object : KLogging()

    fun onStartup() {
        context.getBean(InfluxDbProps::class.java).also { logger.info("influxDbProps -> {}", it) }
        context.getBean(GitlabConfig::class.java).also { logger.info("gitlabProps -> {}", it) }
    }
}

class LogGitInfoOnStartup(val gitProperties: GitProperties?) {

    companion object : KLogging()

    fun onStartup() {
        gitProperties?.apply { logger.info("{}@{}", branch, commitId) }
    }
}

class LogVersionOnStartup(val buildProperties: BuildProperties?) {

    companion object : KLogging()

    fun onStartup() {
        buildProperties?.apply { logger.info("{}:{}:{}@{}", group, artifact, version, time) }
    }
}
