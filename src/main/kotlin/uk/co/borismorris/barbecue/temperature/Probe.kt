package uk.co.borismorris.barbecue.temperature

data class Probe(
        val id: UShort,
        val present: Boolean,
        val temperature: Double
)

fun parseTemperatureData(data: String): Sequence<Probe> {
    val split = data.splitToSequence(';')
    return split.filter { it.isNotEmpty() }.map(::parseProbe)
}

private fun parseProbe(data: String): Probe {
    if (data.length < 3) throw IllegalArgumentException("Data too short")
    val id = data.substring(0, 1).toUShort()
    val present = data.substring(1, 2).toUInt() == 1u
    val tempString = data.substring(2)
    val temperature = if (tempString == "nan") 0.0 else tempString.toDouble()
    return Probe(id, present, temperature)
}
