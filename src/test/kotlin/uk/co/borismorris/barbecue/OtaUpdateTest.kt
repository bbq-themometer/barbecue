package uk.co.borismorris.barbecue

import com.github.jenspiegsa.wiremockextension.ConfigureWireMock
import com.github.jenspiegsa.wiremockextension.InjectServer
import com.github.jenspiegsa.wiremockextension.WireMockExtension
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.Options
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.test.web.reactive.server.WebTestClient
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header

@ExtendWith(WireMockExtension::class)
internal class OtaUpdateTest {
    lateinit var context: ConfigurableApplicationContext
    lateinit var client: WebTestClient

    @InjectServer
    lateinit var serverMock: WireMockServer

    @ConfigureWireMock
    @SuppressWarnings("unused")
    val options = WireMockConfiguration.wireMockConfig()
            .dynamicPort()
            .usingFilesUnderClasspath("uk/co/borismorris/barbecue/gitlab/otaupdate/wiremock")
            .useChunkedTransferEncoding(Options.ChunkedEncodingPolicy.NEVER)
            .notifier(ConsoleNotifier(false))

    @BeforeEach
    fun init() {
        val args = arrayOf(
                "--gitlab.token=tokentoken",
                "--gitlab.url=http://localhost:${serverMock.port()}/gitlab",
                "--influxdb.url=http://localhost:${serverMock.port()}/influx"
        )
        context = app.run(args, "test", true)
        client = WebTestClient.bindToApplicationContext(context).build()
    }

    @AfterEach
    fun tearDown() {
        context.close()
    }

    @Test
    fun `When the same version is requested then a not modified response is sent`() {
        client.get()
                .uri("/otaupdate")
                .header(Esp8266Header.SKETCH_VERSION.header, "versionversion")
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.NOT_MODIFIED)

        assertThat(getAllScenarios()).hasOnlyOneElementSatisfying { scenario ->
            assertThat(scenario.state).isEqualTo("Requested pipeline details")
        }
    }

    @Test
    fun `When a different version is requested then the firmware is sent`() {
        client.get()
                .uri("/otaupdate")
                .header(Esp8266Header.SKETCH_VERSION.header, "notversion")
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.OK)
                .expectBody()
                .consumeWith {
                    val data = it.responseBody!!.decodeToString()
                    assertThat(data)
                            .hasSize(3600)
                            .startsWith("Lorem ipsum dolor sit amet")
                            .endsWith("quam vulputate dignissim suspendisse.")
                }

        assertThat(getAllScenarios()).hasOnlyOneElementSatisfying { scenario ->
            assertThat(scenario.state).isEqualTo("Requested firmware")
        }
    }
}
