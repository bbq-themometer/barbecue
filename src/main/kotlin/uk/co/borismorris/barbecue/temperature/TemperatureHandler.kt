package uk.co.borismorris.barbecue.temperature

import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.awaitFirst
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.awaitBody
import uk.co.borismorris.reactiveflux.InfluxClient
import uk.co.borismorris.reactiveflux.InfluxMeasurementConverter
import uk.co.borismorris.reactiveflux.measurement
import java.time.Instant

class TemperatureHandler(private val influxClient: InfluxClient) {
    companion object : KLogging()

    suspend fun putTemperature(request: ServerRequest): ServerResponse {
        val now = Instant.now()
        val data = request.awaitBody<String>()
        logger.debug("Got temperature data {}", data)
        publishToInflux(data, now)
        return ServerResponse.status(HttpStatus.NO_CONTENT).build().awaitFirst()
    }

    private suspend fun publishToInflux(data: String, now: Instant) {
        val reading = TemperatureReading(now, parseTemperatureData(data))
        logger.debug("Parsed temperature reading {}", reading)
        influxClient.convertAndWrite(flowOf(reading))
                .collect {
                    logger.debug("Published to Influx {}", it)
                }
    }
}

data class TemperatureReading(val now: Instant, val probes: Sequence<Probe>)

class TemperatureReadingConverter : InfluxMeasurementConverter {
    override suspend fun convert(source: Any) = convert(source as TemperatureReading)

    private fun convert(source: TemperatureReading) = source.probes.asFlow()
            .filter { it.present }
            .map { convert(it, source.now) }

    private fun convert(source: Probe, now: Instant) = measurement("temperature") {
        time(now)
        source.apply {
            field("temperature") to temperature
            tag("probe") to id
            tag("present") to present
        }
    }

    override fun canConvert(source: Any) = source is TemperatureReading
}
