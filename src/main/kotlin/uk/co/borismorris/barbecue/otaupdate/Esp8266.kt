package uk.co.borismorris.barbecue.otaupdate

import org.springframework.web.reactive.function.server.ServerRequest
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.AP_MAC
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.CHIP_SIZE
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.FREE_SPACE
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.MODE
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.SDK
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.SKETCH_MD5
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.SKETCH_SIZE
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.SKETCH_VERSION
import uk.co.borismorris.barbecue.otaupdate.Esp8266Header.STA_MAC

enum class Esp8266Header(val header: String) {
    STA_MAC("x-ESP8266-STA-MAC"),
    AP_MAC("x-ESP8266-AP-MAC"),
    FREE_SPACE("x-ESP8266-free-space"),
    SKETCH_SIZE("x-ESP8266-sketch-size"),
    SKETCH_MD5("x-ESP8266-sketch-md5"),
    CHIP_SIZE("x-ESP8266-chip-size"),
    SDK("x-ESP8266-sdk-version"),
    MODE("x-ESP8266-mode"),
    SKETCH_VERSION("x-ESP8266-version");
}

data class Esp8266Headers(
        val staMac: String,
        val apMac: String,
        val freeSpace: UInt,
        val chipSize: UInt,
        val sdk: String,
        val mode: String,
        val sketch: Esp8266Sketch)

data class Esp8266Sketch(
        val size: UInt,
        val md5: String,
        val version: String)

fun ServerRequest.Headers.parseEsp8266Headers() = Esp8266Headers(
        getHeader(STA_MAC, ""),
        getHeader(AP_MAC, ""),
        getHeader(FREE_SPACE, 0u, String::toUInt),
        getHeader(CHIP_SIZE, 0u, String::toUInt),
        getHeader(SDK, ""),
        getHeader(MODE, ""),
        Esp8266Sketch(
                getHeader(SKETCH_SIZE, 0u, String::toUInt),
                getHeader(SKETCH_MD5, ""),
                getHeader(SKETCH_VERSION, "")
        )
)

fun ServerRequest.Headers.getHeader(header: Esp8266Header, default: String) = getHeader(header) ?: default

inline fun <reified T> ServerRequest.Headers.getHeader(header: Esp8266Header, default: T, converter: (String) -> T) = getHeader(header)?.let(converter) ?: default

fun ServerRequest.Headers.getHeader(header: Esp8266Header) = header(header.header).firstOrNull()
