import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmCompile

plugins {
    java
    application
    idea
    kotlin("jvm") version "1.3.72"
    kotlin("plugin.spring") version "1.3.72"
    id("com.google.cloud.tools.jib") version "2.4.0"
    id("com.pasam.gradle.buildinfo") version "0.1.3"
    id("com.github.ben-manes.versions") version "0.28.0"
}

repositories {
    jcenter()
    maven("https://repo.spring.io/milestone")
    maven("https://gitlab.com/api/v4/projects/10133668/packages/maven")
    maven("https://gitlab.com/api/v4/projects/19359150/packages/maven")
    maven("https://jitpack.io")
}

val mainClassKt = "uk.co.borismorris.barbecue.BbqApplicationKt"
val jvmArgs = listOfNotNull(
        "-Dspring.profiles.active=prod",
        "-XX:+PrintCommandLineFlags",
        "-XX:+DisableExplicitGC",
        "-Xlog:gc+stats=info::t,u,l,tg")

java {
    sourceCompatibility = JavaVersion.VERSION_14
    targetCompatibility = JavaVersion.VERSION_14
}

application {
    mainClassName = mainClassKt
    applicationDefaultJvmArgs = jvmArgs + listOfNotNull(
            "-XX:+DisableAttachMechanism",
            "-Dcom.sun.management.jmxremote",
            "-Dcom.sun.management.jmxremote.port=9000",
            "-Dcom.sun.management.jmxremote.local.only=false",
            "-Dcom.sun.management.jmxremote.authenticate=false",
            "-Dcom.sun.management.jmxremote.ssl=false",
            "-Dcom.sun.management.jmxremote.rmi.port=9000",
            "-Djava.rmi.server.hostname=127.0.0.1")
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "13"
        freeCompilerArgs = listOfNotNull(
                "-Xjsr305=strict",
                "-Xjvm-default=enable",
                "-Werror",
                "-progressive",
                "-Xopt-in=kotlin.RequiresOptIn",
                "-Xopt-in=kotlin.ExperimentalUnsignedTypes",
                "-Xopt-in=kotlin.ExperimentalStdlibApi",
                "-Xopt-in=kotlinx.coroutines.FlowPreview",
                "-Xopt-in=kotlinx.coroutines.ExperimentalCoroutinesApi")
    }
}

configurations.all {
    exclude(module = "spring-boot-starter-logging")
    exclude(module = "javax.annotation-api")
    exclude(module = "jakarta.validation-api")
    exclude(module = "hibernate-validator")
}

val springBootVersion = "2.3.1.RELEASE"
val springBootBom = "org.springframework.boot:spring-boot-dependencies:$springBootVersion"

dependencies {
    implementation(platform(springBootBom))

    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("org.springframework.fu:spring-fu-kofu:0.3.1")

    implementation("com.github.vladimir-bukhtoyarov:bucket4j-core:4.10.0")

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("io.projectreactor:reactor-core")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("com.github.ben-manes.caffeine:caffeine:2.8.0")

    implementation("uk.co.borismorris.reactiveflux:reactive-flux:0.5.14")

    implementation("io.github.microutils:kotlin-logging:1.7.10")
    implementation("be.olsson:slack-appender:1.3.0")
    implementation("uk.co.borismorris.slack.webclient:webclient-slack-client:0.0.2")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.26.3")
    testImplementation("com.github.JensPiegsa:wiremock-extension:0.4.0")
    testImplementation("org.mockito:mockito-junit-jupiter")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation(kotlin("test-junit5"))

    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.withType<Test> {
    useJUnitPlatform()

    testLogging {
        exceptionFormat = FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}

jib {
    from {
        image = "azul/zulu-openjdk-alpine:14"
    }
    container {
        appRoot = "/opt/bbq"
        workingDirectory = "/opt/bbq"
        jvmFlags = jvmArgs + listOfNotNull(
                "-XX:InitialRAMPercentage=50",
                "-XX:MaxRAMPercentage=85")
        mainClass = mainClassKt
        creationTime = "USE_CURRENT_TIMESTAMP"
    }
}
