package uk.co.borismorris.barbecue.gitlab.webclient

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.Weigher
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.future.await
import org.springframework.core.io.Resource
import org.springframework.http.MediaType.APPLICATION_JSON
import org.springframework.http.MediaType.APPLICATION_OCTET_STREAM
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToFlow
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import uk.co.borismorris.barbecue.gitlab.Artefact
import uk.co.borismorris.barbecue.gitlab.GitlabApi
import uk.co.borismorris.barbecue.gitlab.GitlabConfig
import uk.co.borismorris.barbecue.gitlab.Job
import uk.co.borismorris.barbecue.gitlab.Pipeline

private const val TOKEN = "PRIVATE-TOKEN"
private const val MEGABYTE = 1_000_000L

class WebclientGitlabClient(config: GitlabConfig, webclientBuilder: WebClient.Builder) : GitlabApi {

    private val artefactCache = Caffeine.newBuilder()
            .weigher(Weigher<CacheKey, Artefact> { _, value -> value.resource.contentLength().toInt() })
            .maximumWeight(MEGABYTE)
            .buildAsync<CacheKey, Artefact>()

    private val webClient = webclientBuilder
            .baseUrl(config.url.toString())
            .defaultHeader(TOKEN, config.token)
            .defaultUriVariables(mutableMapOf("project" to config.project))
            .build()

    override suspend fun mostRecentPipeline() = webClient.get()
            .uri("/projects/{project}/pipelines") {
                it.queryParam("per_page", 1)
                        .queryParam("page", 1)
                        .build()
            }
            .accept(APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<Pipeline>()
            .first()

    override suspend fun jobs(pipeline: Pipeline) = webClient.get()
            .uri("/projects/{project}/pipelines/{pipeline-id}/jobs") {
                it.build(mutableMapOf("pipeline-id" to pipeline.id))
            }
            .accept(APPLICATION_JSON)
            .retrieve()
            .bodyToFlow<Job>()

    override suspend fun artefact(job: Job, path: String?) = fileInArtefact(job, path ?: "")

    private suspend fun fileInArtefact(job: Job, path: String): Artefact = if (path.startsWith("/"))
        fileInArtefact(job, path.substring(1))
    else
        artefactCache.get(CacheKey(job.id, path)) { key, exec ->
            Mono.defer {
                webClient.get()
                        .uri("/projects/{project}/jobs/{job-id}/artifacts/${key.path}") {
                            it.build(mutableMapOf("job-id" to key.jobId))
                        }
                        .accept(APPLICATION_OCTET_STREAM)
                        .retrieve()
                        .bodyToMono<Resource>()
                        .map { Artefact(it) }
            }.subscribeOn(Schedulers.fromExecutor(exec)).toFuture()
        }.await()

    data class CacheKey(val jobId: String, val path: String)
}
