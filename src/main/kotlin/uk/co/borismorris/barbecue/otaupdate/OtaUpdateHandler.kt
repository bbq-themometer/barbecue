package uk.co.borismorris.barbecue.otaupdate

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.reactive.awaitFirst
import mu.KLogging
import org.springframework.http.HttpHeaders.CONTENT_DISPOSITION
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_OCTET_STREAM
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import uk.co.borismorris.barbecue.gitlab.GitlabApi
import uk.co.borismorris.barbecue.gitlab.Pipeline

class OtaUpdateHandler(private val gitlabApi: GitlabApi) {
    companion object : KLogging()

    suspend fun otaUpdate(request: ServerRequest): ServerResponse {
        val headers = request.headers().parseEsp8266Headers()
        return getFirmwareUpdate(headers)
    }

    private suspend fun noFirwareUpdateAvailable() = ServerResponse
            .status(HttpStatus.NOT_MODIFIED)
            .build()
            .awaitFirst()

    private suspend fun firmwareUpdateAvailable(pipeline: Pipeline) = getFirmwareUpdate(pipeline).let {
        logger.info("Updating firmware to {}.", pipeline.sha)
        ServerResponse
                .ok()
                .contentType(APPLICATION_OCTET_STREAM)
                .header(CONTENT_DISPOSITION, "attachment; filename=\"firmware.bin\"")
                .bodyValueAndAwait(it.resource)
    }

    private suspend fun getFirmwareUpdate(pipeline: Pipeline) = gitlabApi.jobs(pipeline)
            .first()
            .let { gitlabApi.artefact(it, ".pio/build/huzzah/firmware.bin") }

    private suspend fun getFirmwareUpdate(esp8266Headers: Esp8266Headers): ServerResponse = gitlabApi.mostRecentPipeline().let {
        if (checkVersion(it, esp8266Headers))
            firmwareUpdateAvailable(it)
        else
            noFirwareUpdateAvailable()
    }

    private fun checkVersion(pipeline: Pipeline, esp8266Headers: Esp8266Headers): Boolean {
        logger.debug("Compare pipeline {} to headers {}", pipeline, esp8266Headers)
        val updateAvailable = !pipeline.sha.startsWith(esp8266Headers.sketch.version)
        if (updateAvailable) {
            logger.info("Update available from {} to {}.", esp8266Headers.sketch.version, pipeline.sha)
        }
        return updateAvailable
    }
}
