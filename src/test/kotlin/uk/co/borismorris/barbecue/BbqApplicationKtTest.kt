package uk.co.borismorris.barbecue

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.http.HttpStatus
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.WebTestClient.bindToApplicationContext

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class BbqApplicationKtTest {
    lateinit var context: ConfigurableApplicationContext
    lateinit var client: WebTestClient

    @BeforeEach
    fun init() {
        val args = arrayOf("--gitlab.token=tokentoken")
        context = app.run(args, "test", true)
        client = bindToApplicationContext(context).build()
    }

    @AfterEach
    fun tearDown() {
        context.close()
    }

    @Test
    fun `When the root endpoint is requested, then 404 is returned`() {
        client.get().uri("/").exchange()
                .expectStatus().isEqualTo(HttpStatus.NOT_FOUND)
    }
}
